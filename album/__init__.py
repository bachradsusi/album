import os

import stat

from flask import Flask, Response, url_for, render_template

from PIL import Image

import glob

app = Flask(__name__)
app.config['DEBUG'] = True  # TODO: disable before deploying on production server

DATA_DIR = 'data'
FOTO_DATA_DIR = 'data/foto'

@app.route('/<gallery>/')
def album(gallery):
    fotky = []
    if not os.path.exists('%s/nahledy/%s' % (DATA_DIR, gallery)):
        os.mkdir('%s/nahledy/%s' % (DATA_DIR, gallery))
    cesty = glob.glob('%s/%s/*' % (FOTO_DATA_DIR, gallery))
    cesty.sort()
    for cesta in cesty:
        try:
            im = Image.open(cesta)
        except Exception:
            continue
        foto = cesta.split('/')[-1]
        thumbnail = '%s/nahledy/%s/%s' % (DATA_DIR, gallery, foto)
        if not os.path.exists(thumbnail) or os.stat(cesta)[stat.ST_CTIME] > os.stat(thumbnail)[stat.ST_CTIME]:
            if im.size[0] > im.size[1]:
                box = (
                    (im.size[0] - im.size[1]) / 2 , 0,
                    (im.size[0] - im.size[1]) / 2 + im.size[1], im.size[1]
                )
            else:
                box = (
                    0, (im.size[1] - im.size[0]) / 2 ,
                    im.size[0], (im.size[1] - im.size[0]) / 2 + im.size[0],
                )
            reg = im.crop(box)
            reg.thumbnail((120,120))
            reg.save(thumbnail, "JPEG")
        fotky.append(
            (url_for('stranka', gallery=gallery, foto=foto ),
            url_for('nahled', gallery=gallery, foto=foto ))
        )
    return render_template(
        'album.html', fotky=fotky, galerie=gallery,
    )


@app.route('/stranka/<gallery>/<foto>')
def stranka(gallery, foto):
    fotky = glob.glob('%s/%s/*' % (FOTO_DATA_DIR, gallery))
    fotky.sort()

    foto_url = (
        url_for('stranka', gallery=gallery, foto=foto ),
        url_for('nahled', gallery=gallery, foto=foto ),
        url_for('foto', gallery=gallery, foto=foto )
    )

    foto_prev = fotky[0].split('/')[-1]
    foto_prev_url = (
        url_for('stranka', gallery=gallery, foto=foto_prev ),
        url_for('nahled', gallery=gallery, foto=foto_prev )
    )
    foto_next = fotky[-1].split('/')[-1]
    foto_next_url = (
        url_for('stranka', gallery=gallery, foto=foto_next ),
        url_for('nahled', gallery=gallery, foto=foto_next )
    )
    i = 0
    while i < len(fotky):
        if fotky[i].split('/')[-1] == foto:
            if i > 0:
                foto_prev = fotky[i - 1].split('/')[-1]
                foto_prev_url = (
                    url_for('stranka', gallery=gallery, foto=foto_prev ),
                    url_for('nahled', gallery=gallery, foto=foto_prev )
                )
            if i < len(fotky) - 1:
                foto_next = fotky[i + 1].split('/')[-1]
                foto_next_url = (
                    url_for('stranka', gallery=gallery, foto=foto_next ),
                    url_for('nahled', gallery=gallery, foto=foto_next )
                )
        i = i + 1
    return render_template(
        'stranka.html', foto=foto_url, galerie=gallery,
        foto_prev=foto_prev_url, foto_next=foto_next_url
    )


@app.route('/nahled/<gallery>/<foto>')
def nahled(gallery, foto):
    with open('%s/nahledy/%s/%s' % (DATA_DIR, gallery, foto), 'rb') as f:
        response = f.read()
    return Response(response=response, status=200, headers={ 'Content-Type': 'image/jpeg'} )


@app.route('/foto/<gallery>/<foto>')
def foto(gallery, foto):
    with open('%s/%s/%s' % (FOTO_DATA_DIR, gallery, foto), 'rb') as f:
        response = f.read()
    return Response(response=response, status=200, headers={ 'Content-Type': 'image/jpeg'} )



