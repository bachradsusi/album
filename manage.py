#!/usr/bin/env python

import os

import flask
from flask.ext.script import Manager, Command

from album import app

manager = Manager(app)

if __name__ == '__main__':
    manager.run()
